@ECHO OFF

IF NOT EXIST .\cmake_visual_studio.bat ECHO This script must be executed at the root directory of the project. && EXIT /b 1

IF "%1"=="" ECHO The path to DynamoRIO library folder must be specified. && EXIT /b 1

SET abs_dynamorio_path=%~f1

if NOT EXIST "%abs_dynamorio_path%\cmake" ECHO Invalid DynamoRIO library folder path. && EXIT /b 1

IF EXIST .\build (
  RMDIR /S /Q .\build
  IF %errorlevel% NEQ 0 ECHO Failed to delete the build directory. Make sure the directory is not being used by another process. && EXIT /b 1
)

MKDIR .\build
IF %errorlevel% NEQ 0 ECHO Failed to create the build directory. && EXIT /b 1

CD .\build
IF %errorlevel% NEQ 0 ECHO Unable to change directory to the build directory. && EXIT /b 1

cmake -G"Visual Studio 10" -DDynamoRIO_DIR="%abs_dynamorio_path%\cmake" ../src
IF %errorlevel% NEQ 0 ECHO cmake failed. && EXIT /b 1

CD ..
