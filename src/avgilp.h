#ifndef AVGILP_H
#define AVGILP_H

#include <stdint.h>
#include "dr_api.h"

#define REG_MASK_FULL UINT32_MAX
#define REG_MASK_EMPTY 0

#define REG_MASK_AL 1
#define REG_MASK_AH 2
#define REG_MASK_AX 3
#define REG_MASK_EAX 15

#define REG_MASK_BL 1 << 4
#define REG_MASK_BH 2 << 4
#define REG_MASK_BX 3 << 4
#define REG_MASK_EBX 15 << 4

#define REG_MASK_CL 1 << 8
#define REG_MASK_CH 2 << 8
#define REG_MASK_CX 3 << 8
#define REG_MASK_ECX 15 << 8

#define REG_MASK_DL 1 << 12
#define REG_MASK_DH 2 << 12
#define REG_MASK_DX 3 << 12
#define REG_MASK_EDX 15 << 12

#define REG_MASK_DI 3 << 16
#define REG_MASK_EDI 15 << 16

#define REG_MASK_SI 3 << 20
#define REG_MASK_ESI 15 << 20

#define REG_MASK_SP 3 << 24
#define REG_MASK_ESP 15 << 24

#define REG_MASK_BP 3 << 28
#define REG_MASK_EBP 15 << 28

struct Node;

typedef struct NodePointerArray {
  //invariant: no duplicate entry
  unsigned int array_size;
  struct Node **node_array;
  unsigned int node_count;
} NodePointerArray_t;


typedef struct Node{

  unsigned int execution_order; //sched(n)

  NodePointerArray_t successor_pointer_array;
  NodePointerArray_t predecessor_pointer_array;

  //value
  uint32_t affects;
  uint32_t depends_on;
  int last_visit_key;

  char text[100];
} Node_t;

void insert_node_pointer( NodePointerArray_t *node_array, Node_t *to_insert );
void remove_node_pointer( NodePointerArray_t *node_array, Node_t * to_remove );
void clear_node_pointer_array( NodePointerArray_t *node_array );
void copy_node_pointer_array( NodePointerArray_t *to, NodePointerArray_t *from );
void initialize_node_pointer_array( NodePointerArray_t *node_array );
void delete_node_pointer_array( NodePointerArray_t *node_array );
void initialize_node_pointer_array_with( NodePointerArray_t *node_array, NodePointerArray_t *with );
void calculate_dependency( Node_t *head, uint32_t depends_on, NodePointerArray_t *current_dependency, int visit_key );
uint32_t get_reg_mask(reg_id_t reg);

#ifdef _MSC_VER

static __forceinline void *malloc( size_t size ) {
  size_t *block;
  
  block = (size_t *)dr_global_alloc( size + sizeof(size_t));
  block[0] = size;
  block++;
  return block;
}

static __forceinline void *realloc( void * block, size_t new_size ) {
  size_t *old = (size_t *)block;
  size_t *dest = dr_global_alloc( new_size + sizeof( size_t ));
  
  dest[0] = new_size;
  old--;

  memcpy( &dest[1], &old[1], old[0] );
  dr_global_free( old, old[0] + sizeof( size_t ) );

  dest++;
  return dest;  
}

inline __forceinline void free( void *to_free ) {
  size_t *old = (size_t *)to_free;

  old--;
  dr_global_free( old, old[0] + sizeof( size_t ) );
}

#endif

#endif //AVGILP_H
