#include "dr_api.h"

#include "avgilp.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#ifdef WINDOWS
# define DISPLAY_STRING(msg) dr_messagebox(msg)
#else
# define DISPLAY_STRING(msg) dr_printf("%s\n", msg);
#endif

#define NULL_TERMINATE(buf) buf[(sizeof(buf)/sizeof(buf[0])) - 1] = '\0'

uint64 global_instrumentation_start_time;
static uint64 global_instruction_count;
static uint64 global_execution_time;
unsigned int global_max_node_count = 100;
uint64 global_maximum_dependency_calculation_time = 0;
uint64 global_maximum_scheduling_time = 0;

static void ilpcount(unsigned int num_instrs, unsigned int execution_time) { global_instruction_count += num_instrs; global_execution_time += execution_time; }
static void event_exit(void);
static dr_emit_flags_t event_basic_block(void *drcontext, void *tag, instrlist_t *bb,
                                         bool for_trace, bool translating);


Node_t *nodes = NULL;
NodePointerArray_t node_array;
NodePointerArray_t nodes_to_explore;
NodePointerArray_t nodes_to_explore_next;

unsigned int node_buffer_overflow_count = 0;
unsigned int bb_count = 0;

static void event_exit(void)
{
  char msg[512];
  int len;
  len = dr_snprintf(msg, sizeof(msg)/sizeof(msg[0]),
    "Instrumentation has been completed (elapsed time: %llu milliseconds). The average instruction level parallelism is %.2f \n",dr_get_milliseconds() - global_instrumentation_start_time, ((float)global_instruction_count)/global_execution_time);
  DR_ASSERT(len > 0);
  NULL_TERMINATE(msg);
  DISPLAY_STRING(msg);

  len = dr_snprintf(msg, sizeof(msg)/sizeof(msg[0]),
    "Out of %d basic blocks, %d were partially instrumented due to the size limit imposed on the preallocated buffer (maximum number of instructions per basic block that can be instrumented: %d). This value can be adjusted by passing in an integer as a parameter. Maximum dependency calculation time: %llu, Maximum scheduling time: %llu\n\n", bb_count, node_buffer_overflow_count, global_max_node_count, global_maximum_dependency_calculation_time, global_maximum_scheduling_time);
  DR_ASSERT(len > 0);
  NULL_TERMINATE(msg);
  DISPLAY_STRING(msg);

  delete_node_pointer_array( &node_array );
  delete_node_pointer_array( &nodes_to_explore );
  delete_node_pointer_array( &nodes_to_explore_next );

  {
    int i;
    for( i=0; i<global_max_node_count + 1; i++ ) {
      delete_node_pointer_array( &nodes[i].predecessor_pointer_array);
      delete_node_pointer_array( &nodes[i].successor_pointer_array);
    }
  }

  free(nodes);
}

DR_EXPORT void dr_init(client_id_t id)
{
  unsigned int max_node_count = strtoul( dr_get_options(id), NULL, 10 );

  global_instrumentation_start_time = dr_get_milliseconds();

  if( max_node_count != 0 )
    global_max_node_count = max_node_count;

  nodes = (Node_t *)malloc( sizeof(Node_t) * ( global_max_node_count +1 )); //add extra node for START

  {
    int i;
    for( i=0; i<global_max_node_count + 1; i++ ) {
      initialize_node_pointer_array( &nodes[i].predecessor_pointer_array);
      initialize_node_pointer_array( &nodes[i].successor_pointer_array);
    }
  }

  initialize_node_pointer_array( &node_array );
  initialize_node_pointer_array( &nodes_to_explore );
  initialize_node_pointer_array( &nodes_to_explore_next );

  /* register events */
  dr_register_exit_event(event_exit);
  dr_register_bb_event(event_basic_block);
}
static dr_emit_flags_t
event_basic_block(void *drcontext, void *tag, instrlist_t *bb,
                  bool for_trace, bool translating)
{
  instr_t *instr;
  unsigned int node_count=0;
  uint64 calculate_dependency_start_time;
  uint64 scheduling_time;

  unsigned int execution_order = 0;

  //dr_printf("in dynamorio_basic_block(tag="PFX")\n", tag);
  //instrlist_disassemble(drcontext, tag, bb, STDOUT);

  bb_count++;

  nodes[0].affects = REG_MASK_FULL;
  nodes[0].depends_on = REG_MASK_EMPTY;
  nodes[0].execution_order = -1;
  nodes[0].last_visit_key = -1;
  clear_node_pointer_array( &nodes[0].predecessor_pointer_array);
  clear_node_pointer_array( &nodes[0].successor_pointer_array);
  strcpy(nodes[0].text, "START" );

  {
    unsigned int i,j;


    for (instr  = instrlist_first(bb), i = 1;
      instr != NULL && i <= global_max_node_count;
      instr = instr_get_next(instr), i++ ) {

        node_count++;

        nodes[i].depends_on = REG_MASK_EMPTY;
        nodes[i].affects = REG_MASK_EMPTY;
        nodes[i].execution_order = -1;
        nodes[i].last_visit_key = -1;
        clear_node_pointer_array( &nodes[i].predecessor_pointer_array);
        clear_node_pointer_array( &nodes[i].successor_pointer_array);
        //instr_disassemble_to_buffer(drcontext, instr, nodes[i].text, sizeof(nodes->text));

        for( j=0; j<instr_num_srcs(instr); j++ ) {
          opnd_t op = instr_get_src(instr, j);
          if( opnd_is_reg(op) ) {
            reg_id_t reg = opnd_get_reg(op);
            nodes[i].depends_on |= get_reg_mask( reg );
          }
        }

        for( j=0; j<instr_num_dsts(instr); j++ ) {
          opnd_t op = instr_get_dst(instr, j);
          if( opnd_is_reg(op) ) {
            reg_id_t reg = opnd_get_reg(op);
            nodes[i].affects |= get_reg_mask( reg );
          }
        }
    }

    if( i > global_max_node_count )
      node_buffer_overflow_count++;

    calculate_dependency_start_time = dr_get_milliseconds();

    for( i=1; i<=node_count; i++ ) {

      

      clear_node_pointer_array( &node_array );

      calculate_dependency( &nodes[0], nodes[i].depends_on, &node_array, i );

      if( node_array.node_count > 1 ) { //if it's dependent on nodes other than START, then we exclude START from the dependence graph.
        remove_node_pointer( &node_array, &nodes[0] );
      }

      for( j=0; j<node_array.node_count; j++){
        insert_node_pointer(&node_array.node_array[j]->successor_pointer_array, &nodes[i] );
        insert_node_pointer(&nodes[i].predecessor_pointer_array, node_array.node_array[j]);
      }
    }

    if( dr_get_milliseconds() - calculate_dependency_start_time > global_maximum_dependency_calculation_time )
      global_maximum_dependency_calculation_time = dr_get_milliseconds() - calculate_dependency_start_time;
  }
    
  //Done populating the data-dependence graph. Now do BFS.
  clear_node_pointer_array( &nodes_to_explore );
  clear_node_pointer_array( &nodes_to_explore_next );

  insert_node_pointer( &nodes_to_explore, &nodes[0] ); //Add START
  nodes[0].execution_order = 0;

  scheduling_time = dr_get_milliseconds();

  while( nodes_to_explore.node_count != 0 ) {

    unsigned int i;

    clear_node_pointer_array( &nodes_to_explore_next );
      
    for( i=0; i<nodes_to_explore.node_count; i++ ) {
      unsigned int j;
      Node_t *node = nodes_to_explore.node_array[i];
      node->execution_order = execution_order;

      for( j=0; j<node->successor_pointer_array.node_count; j++ ) {
        unsigned int k;
        Node_t *successor = node->successor_pointer_array.node_array[j];

        bool ignore_this_successor = false;
        for( k=0; k<successor->predecessor_pointer_array.node_count; k++ ) {
          Node_t *predecessor = successor->predecessor_pointer_array.node_array[k];

          if( predecessor->execution_order == -1 ) {
            ignore_this_successor = true;
            break;
          }
        }

        if( !ignore_this_successor ) {
          insert_node_pointer( &nodes_to_explore_next, successor );
        }
      }
    }

    copy_node_pointer_array( &nodes_to_explore, &nodes_to_explore_next );
    execution_order++;
  }

  if( dr_get_milliseconds() - scheduling_time > global_maximum_scheduling_time )
    global_maximum_scheduling_time = dr_get_milliseconds() - scheduling_time;

  dr_insert_clean_call(drcontext, bb, instrlist_first(bb), 
                        (void *)ilpcount, false /* save fpstate */, 2,
                        OPND_CREATE_INT32(node_count),  OPND_CREATE_INT32(execution_order - 1));

  return DR_EMIT_DEFAULT;
}
