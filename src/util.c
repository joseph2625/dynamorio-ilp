#include "avgilp.h"

#include "dr_api.h"
#include <string.h>

void calculate_dependency( Node_t *head, uint32_t depends_on, NodePointerArray_t *current_dependency, int visit_key ) {

  NodePointerArray_t new_current_dependency;

  head->last_visit_key = visit_key;
  //remove overlapping dependency
  {
    unsigned int i=0;
    for( i=0; i<current_dependency->node_count; i++ ) {
      if( (( current_dependency->node_array[i]->affects | head->affects ) & ~head->affects) ==0 ) { //dependency gets completely dominated
        remove_node_pointer( current_dependency, current_dependency->node_array[i] );
      }
    }
  }

  if( ( depends_on & head->affects ) > 0 )
    insert_node_pointer( current_dependency, head );

  if( head->successor_pointer_array.node_count == 0 )
    return;

  initialize_node_pointer_array(&new_current_dependency);

  {
    unsigned int i,j;
    for( i=0; i<head->successor_pointer_array.node_count; i++ ) {
      NodePointerArray_t child_dependency;

      if( head->successor_pointer_array.node_array[i]->last_visit_key == visit_key )
        continue;

      initialize_node_pointer_array_with( &child_dependency, current_dependency);
      calculate_dependency( head->successor_pointer_array.node_array[i], depends_on, &child_dependency, i );

      for( j=0; j< child_dependency.node_count; j++ ) {
        insert_node_pointer( &new_current_dependency, child_dependency.node_array[j] );
      }

      delete_node_pointer_array( &child_dependency );
    }
  }

  copy_node_pointer_array(current_dependency,&new_current_dependency);
  delete_node_pointer_array( &new_current_dependency );
}

void insert_node_pointer( NodePointerArray_t *node_array, Node_t *to_insert ) {
  //check for duplicates
  unsigned int i;
  for( i=0; i<node_array->node_count; i++ ) {
    if( node_array->node_array[i] == to_insert )
      return;
  }

  //no duplicates
  if( node_array->node_count + 1 > node_array->array_size ) {
    node_array->array_size *= 2;
    node_array->node_array = (Node_t **)realloc( node_array->node_array, node_array->array_size * sizeof(Node_t *) );
  }

  //array is guaranteed to be big enough
  node_array->node_array[node_array->node_count] = to_insert;

  //increase node count
  node_array->node_count++;
}

void remove_node_pointer( NodePointerArray_t *node_array, Node_t * to_remove ) {

  unsigned int l=0, removed=0;
  unsigned int i;

  for( i=0; i<node_array->node_count; i++ ) {
    if( node_array->node_array[i] != to_remove )
      node_array->node_array[l++] = node_array->node_array[i];
    else
      removed++;
  }

  node_array->node_count -= removed;
}

void clear_node_pointer_array( NodePointerArray_t *node_array ) {
  node_array->node_count = 0;
}

void copy_node_pointer_array( NodePointerArray_t *to, NodePointerArray_t *from ) {
  if( to->array_size < from->array_size ) {
    to->node_array = (Node_t **)realloc( to->node_array, sizeof(Node_t *) * from->array_size );
    to->array_size = from->array_size;
  }
  to->node_count = from->node_count;
  memcpy( to->node_array, from->node_array, sizeof(Node_t *) * from->node_count );
}
void initialize_node_pointer_array( NodePointerArray_t *node_array ) {
  node_array->array_size = 8;
  node_array->node_array = (Node_t **)malloc( sizeof(Node_t *) * 8 );
  node_array->node_count = 0;
}
void delete_node_pointer_array( NodePointerArray_t *node_array ) {
  free( node_array->node_array );
}

void initialize_node_pointer_array_with( NodePointerArray_t *node_array, NodePointerArray_t *with ) {
  node_array->array_size = with->array_size;
  node_array->node_count = with->node_count;
  node_array->node_array = (Node_t **)malloc( sizeof(Node_t *) * with->array_size );
  memcpy( node_array->node_array, with->node_array, sizeof(Node_t *) * with->node_count);
}

uint32_t get_reg_mask(reg_id_t reg) {
  switch(reg) {
  case DR_REG_AH:
    return REG_MASK_AH;
  case DR_REG_AL:
    return REG_MASK_AL;
  case DR_REG_AX:
    return REG_MASK_AX;
  case DR_REG_EAX:
    return REG_MASK_EAX;
  case DR_REG_BH:
    return REG_MASK_BH;
  case DR_REG_BL:
    return REG_MASK_BL;
  case DR_REG_BX:
    return REG_MASK_BX;
  case DR_REG_EBX:
    return REG_MASK_EBX;
  case DR_REG_CH:
    return REG_MASK_CH;
  case DR_REG_CL:
    return REG_MASK_CL;
  case DR_REG_CX:
    return REG_MASK_CX;
  case DR_REG_ECX:
    return REG_MASK_ECX;
  case DR_REG_DH:
    return REG_MASK_DH;
  case DR_REG_DL:
    return REG_MASK_DL;
  case DR_REG_DX:
    return REG_MASK_DX;
  case DR_REG_EDX:
    return REG_MASK_EDX;
  case DR_REG_SI:
    return REG_MASK_SI;
  case DR_REG_ESI:
    return REG_MASK_ESI;
  case DR_REG_DI:
    return REG_MASK_DI;
  case DR_REG_EDI:
    return REG_MASK_EDI;
  case DR_REG_SP:
    return REG_MASK_SP;
  case DR_REG_BP:
    return REG_MASK_BP;
  case DR_REG_ESP:
    return REG_MASK_ESP;
  case DR_REG_EBP:
    return REG_MASK_EBP;
  default:
    return REG_MASK_EMPTY;
  }
}
